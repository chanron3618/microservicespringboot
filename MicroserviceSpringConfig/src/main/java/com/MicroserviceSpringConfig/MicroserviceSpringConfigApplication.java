package com.MicroserviceSpringConfig;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroserviceSpringConfigApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroserviceSpringConfigApplication.class, args);
	}

}
