package com.MicroserviceSpringDiscovery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class MicroserviceSpringDiscoveryApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroserviceSpringDiscoveryApplication.class, args);
	}

}
